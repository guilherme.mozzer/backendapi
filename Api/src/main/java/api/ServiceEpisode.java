package api;

import bo.BOEpisode;
import bo.BOUser;
import fw.Cache;
import to.TOEpisode;
import to.TOUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("episode")
public class ServiceEpisode {
    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    @Produces("application/json;charset=utf-8")
    public void insert(TOEpisode e, @HeaderParam("token") String token) throws Exception {
        TOUser t = (TOUser)Cache.getCache("user", token);

        if (t != null) {
            if (BOUser.isValidToken(t.getToken())) {

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOEpisode.insert(e);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }


    @PUT
    @Consumes("application/json;charset=utf-8")
    public void update(TOEpisode e, @HeaderParam("token") String token) throws Exception {
        TOUser t = (TOUser)Cache.getCache("user", token);

        if (t != null) {
            if (BOUser.isValidToken(t.getToken())) {

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOEpisode.update(e);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

/*    @GET
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public TOEpisode episode(@PathParam("id") String id)throws Exception{
        return BOEpisode.getEpisode(Integer.parseInt(id));
    }*/

    @GET
    @Path("byMovie/{movieID}")
    @Produces("application/json;charset=utf-8")
    public List<TOEpisode> episodesByMovie(@PathParam("movieID") int movieID)throws Exception{
        return BOEpisode.listEpisodeByMovie(movieID);
    }

    @GET
    @Path("bySeason/{movieID}/{seasonID}")
    @Produces("application/json;charset=utf-8")
    public List<TOEpisode> episodesByMovie(@PathParam("movieID") int movieID, @PathParam("seasonID") int seasonID)throws Exception{
        return BOEpisode.listEpisodeBySeason(movieID, seasonID);
    }

    @DELETE
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public void delete(@PathParam("id") String idEpisode, @HeaderParam("token") String token) throws Exception {
        TOUser t = (TOUser)Cache.getCache("user", token);

        if (t != null) {
            if (BOUser.isValidToken(t.getToken())) {

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")) {
                    BOEpisode.delete(Integer.parseInt(idEpisode));
                } else {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        }else{
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
