package api;

import bo.BOGenderList;
import bo.BOUser;
import fw.Cache;
import to.TOGenderList;
import to.TOUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("genderlist")
public class ServiceGenderList {
    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    @Produces("application/json;charset=utf-8")
    public void insert(TOGenderList gl) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOGenderList.insert(gl);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @GET
    @Produces("application/json;charset=utf-8")
    public List<TOGenderList> genderLists()throws Exception{
        return BOGenderList.getGenderList();
    }

    @GET
    @Path("byMovie/{movieID}")
    @Produces("application/json;charset=utf-8")
    public List<TOGenderList> getByMovie(@PathParam("movieID") String movieID)throws Exception{
        return BOGenderList.getGenderListMovie(Integer.parseInt(movieID));
    }

    @GET
    @Path("byGender/{genderID}")
    @Produces("application/json;charset=utf-8")
    public List<TOGenderList> getByGender(@PathParam("genderID") String genderID)throws Exception{
        return BOGenderList.getGenderListGender(Integer.parseInt(genderID));
    }

    @DELETE
    @Produces("application/json;charset=utf-8")
    public void delete(TOGenderList gl)throws Exception{
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")) {
                    BOGenderList.delete(gl.getMovieID(), gl.getGenderID());
                } else {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
