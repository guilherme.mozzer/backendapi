package api;

import bo.BOMovie;
import bo.BOUser;
import fw.Cache;
import to.TOMovie;
import to.TOMovieGender;
import to.TOUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("movie")
public class ServiceMovie {
    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    @Produces("application/json;charset=utf-8")
    public void insert(TOMovie m) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOMovie.insert(m);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }


    @PUT
    @Consumes("application/json;charset=utf-8")
    public void update(TOMovie m) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOMovie.update(m);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @GET
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public TOMovie movie(@PathParam("id") String id)throws Exception{
        return BOMovie.getMovie(Integer.parseInt(id));
    }

    @GET
    @Produces("application/json;charset=utf-8")
    public List<TOMovie> movies()throws Exception{
        return BOMovie.listMovie();
    }

    @GET
    @Path("bySearch/{search}")
    @Produces("application/json;charset=utf-8")
    public List<TOMovie> getMoviesByTitle(@PathParam("search") String search)throws Exception{
        return BOMovie.listMovieByTitle(search);
    }

    @GET
    @Path("byCategory/{categoryID}")
    @Produces("application/json;charset=utf-8")
    public List<TOMovie> getMoviesByCategory(@PathParam("categoryID") String categoryID)throws Exception{
        return BOMovie.listMovieByCategory(Integer.parseInt(categoryID));
    }

    @GET
    @Path("byGender/{genderID}")
    @Produces("application/json;charset=utf-8")
    public List<TOMovieGender> listMoviesByGender(@PathParam("genderID") String genderID)throws Exception{
        return BOMovie.listMovieByGender(Integer.parseInt(genderID));
    }

    @DELETE
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public void delete(@PathParam("id") String idMovie) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")) {
                    BOMovie.delete(Integer.parseInt(idMovie));
                } else {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        }else{
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
