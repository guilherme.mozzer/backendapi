package api;

import bo.BOUser;
import fw.Cache;
import org.json.JSONObject;
import to.TOUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;

@Path("user")
public class ServiceUser {
    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    @Produces("application/json;charset=utf-8")
    public String insert(TOUser u) throws Exception {
        TOUser t = BOUser.insert(u);
        if (t != null) {
            JSONObject j = new JSONObject();
            j.put("id", t.getId());
            return j.toString();
        } else {
            response.sendError(HttpServletResponse.SC_CONFLICT);
            return null;
        }
    }


    @PUT
    @Consumes("application/json;charset=utf-8")
    public void update(TOUser u) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {
                TOUser t = BOUser.me(r.toString());

                if(t.getId().equals(u.getId()) || t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")) {
                    if (BOUser.update(u)) {
                        response.sendError(HttpServletResponse.SC_ACCEPTED);
                    } else {
                        response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    }
                } else {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

            } else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }


    @POST
    @Path("auth")
    @Consumes("application/json;charset=utf-8")
    @Produces("application/json;charset=utf-8")
    public TOUser auth(TOUser u) throws Exception {

        TOUser t = BOUser.auth(u);
        if (t == null) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        } else{
            Cache.setCache("user", t.getToken(), t, 10);
        }

        return t;

    }

    @GET
    @Path("me")
    @Produces("application/json;charset=utf-8")
    public TOUser me(@HeaderParam("token") String token) throws Exception {

        TOUser r = (TOUser)Cache.getCache("user", token);

        TOUser t = null;

        if (r != null) {
            if (BOUser.isValidToken(r.getToken())) {
                t = BOUser.me(r.getToken());
            } else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        }else{
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }

        return t;
    }


}
