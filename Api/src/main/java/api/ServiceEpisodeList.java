package api;

import bo.BOEpisodeList;
import bo.BOUser;
import fw.Cache;
import to.TOEpisodeList;
import to.TOEpisodeUserList;
import to.TOUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("episodelist")
public class ServiceEpisodeList {
    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    @Produces("application/json;charset=utf-8")
    public void insert(TOEpisodeList el) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

//                TOUser t = BOUser.me(r.toString());

//                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOEpisodeList.insert(el);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
//                } else{
//                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
//                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @GET
    @Produces("application/json;charset=utf-8")
    public List<TOEpisodeUserList> getEpisodeListByUser()throws Exception{
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {
                TOUser t = BOUser.me(r.toString());
                return BOEpisodeList.getEpisodeListByUser(t.getId());
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
                return  null;
            }
        } else{
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return  null;
        }
    }

    @GET
    @Path("{movieID}")
    @Produces("application/json;charset=utf-8")
    public List<TOEpisodeUserList> getEpisodeListByUserAndMovie(@PathParam("movieID") int movieID)throws Exception{
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {
                TOUser t = BOUser.me(r.toString());
                return BOEpisodeList.getEpisodeListByUserAndMovie(t.getId(), movieID);
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
                return  null;
            }
        } else{
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return  null;
        }
    }

    @GET
    @Path("{movieID}/{seasonID}")
    @Produces("application/json;charset=utf-8")
    public List<TOEpisodeUserList> getEpisodeListByUserAndMovieAndSeason(@PathParam("movieID") int movieID, @PathParam("seasonID") int seasonID)throws Exception{
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {
                TOUser t = BOUser.me(r.toString());
                return BOEpisodeList.getEpisodeListByUserAndMovieAndSeason(t.getId(), movieID, seasonID);
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
                return  null;
            }
        } else{
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return  null;
        }
    }

    @DELETE
    @Produces("application/json;charset=utf-8")
    public void delete(TOEpisodeList sl)throws Exception{
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")) {
                    BOEpisodeList.delete(sl.getEpisodeID(), sl.getUserID());
                } else {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
