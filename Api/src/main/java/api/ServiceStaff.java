package api;

import bo.BOStaff;
import bo.BOUser;
import fw.Cache;
import to.TOStaff;
import to.TOUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("staff")
public class ServiceStaff {

    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    @Produces("application/json;charset=utf-8")
    public void insert(TOStaff s) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOStaff.insert(s);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }


    @PUT
    @Consumes("application/json;charset=utf-8")
    public void update(TOStaff s) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOStaff.update(s);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @GET
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public TOStaff staff(@PathParam("id") String id)throws Exception{
        return BOStaff.getStaff(Integer.parseInt(id));
    }

    @GET
    @Path("list/{search}")
    @Produces("application/json;charset=utf-8")
    public List<TOStaff> staffs(@PathParam("search") String search)throws Exception{
        if(search.equals("all"))
            search = "";
        return BOStaff.listStaff(search);
    }

    @DELETE
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public void delete(@PathParam("id") String idStaff) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")) {
                    BOStaff.delete(Integer.parseInt(idStaff));
                } else {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        }else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
