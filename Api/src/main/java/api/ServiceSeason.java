package api;

import bo.BOSeason;
import bo.BOUser;
import fw.Cache;
import to.TOSeason;
import to.TOUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("season")
public class ServiceSeason {

    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    @Produces("application/json;charset=utf-8")
    public void insert(TOSeason s) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());
                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOSeason.insert(s);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }


    @PUT
    @Consumes("application/json;charset=utf-8")
    public void update(TOSeason s) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOSeason.update(s);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

/*    @GET
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public TOSeason season(@PathParam("id") String id)throws Exception{
        return BOSeason.getSeason(Integer.parseInt(id));
    }*/

    @GET
    @Produces("application/json;charset=utf-8")
    public List<TOSeason> seasons()throws Exception {
        Object r = Cache.getCache("season", "list");

        List<TOSeason> l;

        if (r != null) {
            l = (List<TOSeason>)r;
        } else{
            l = BOSeason.listSeason();
            Cache.setCache("season", "list", l, 120);
        }

        return l;
    }

    @DELETE
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public void delete(@PathParam("id") String idSeason) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")) {
                    BOSeason.delete(Integer.parseInt(idSeason));
                } else {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        }else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
