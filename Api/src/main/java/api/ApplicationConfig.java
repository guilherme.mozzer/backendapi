package api;


import javax.ws.rs.core.Application;
import java.util.Set;

@javax.ws.rs.ApplicationPath("v1")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {

        resources.add(ServiceCategory.class);
        resources.add(ServiceEpisode.class);
        resources.add(ServiceEpisodeList.class);
        resources.add(ServiceGender.class);
        resources.add(ServiceGenderList.class);
        resources.add(ServiceMovie.class);
        resources.add(ServiceOccupation.class);
        resources.add(ServicePersona.class);
        resources.add(ServiceSeason.class);
        resources.add(ServiceStaff.class);
        resources.add(ServiceStaffList.class);
        resources.add(ServiceStudio.class);
        resources.add(ServiceUser.class);
        resources.add(ServiceVersion.class);

    }

}