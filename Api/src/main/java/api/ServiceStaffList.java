package api;

import bo.BOStaffList;
import bo.BOUser;
import fw.Cache;
import to.TOStaffList;
import to.TOUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("stafflist")
public class ServiceStaffList {
    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    @Produces("application/json;charset=utf-8")
    public void insert(TOStaffList sl) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOStaffList.insert(sl);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @GET
    @Produces("application/json;charset=utf-8")
    public List<TOStaffList> staffLists()throws Exception{
        return BOStaffList.getStaffList();
    }

    @GET
    @Path("byStaff/{idStaff}")
    @Produces("application/json;charset=utf-8")
    public List<TOStaffList> getByStaff(@PathParam("idStaff") String idStaff)throws Exception{
        return BOStaffList.getStaffListByStaff(Integer.parseInt(idStaff));
    }

    @GET
    @Path("byMovie/{idMovie}")
    @Produces("application/json;charset=utf-8")
    public List<TOStaffList> getByMovie(@PathParam("idMovie") String idMovie)throws Exception{
        return BOStaffList.getStaffListByMovie(Integer.parseInt(idMovie));
    }

    @GET
    @Path("byPersona/{idPersona}")
    @Produces("application/json;charset=utf-8")
    public List<TOStaffList> getByPersona(@PathParam("idPersona") String idPersona)throws Exception{
        return BOStaffList.getStaffListByPersona(Integer.parseInt(idPersona));
    }

    @GET
    @Path("byMovieAndStaff/{idMovie}/{idStaff}")
    @Produces("application/json;charset=utf-8")
    public List<TOStaffList> getByStaffAndMovie(@PathParam("idMovie") String idMovie, @PathParam("idStaff") String idStaff)throws Exception{
        return BOStaffList.getStaffListByMovieAndStaff(Integer.parseInt(idMovie), Integer.parseInt(idStaff));
    }

    @DELETE
    @Produces("application/json;charset=utf-8")
    public void delete(TOStaffList sl)throws Exception{
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")) {
                    BOStaffList.delete(sl.getMovieID(), sl.getStaffID(), sl.getPersonaID());
                } else {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
