package api;

import bo.BOGender;
import bo.BOUser;
import fw.Cache;
import org.json.JSONObject;
import to.TOGender;
import to.TOUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("gender")
public class ServiceGender {

    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    @Produces("application/json;charset=utf-8")
    public void insert(TOGender g) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOGender.insert(g);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }


    @PUT
    @Consumes("application/json;charset=utf-8")
    public void update(TOGender g) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOGender.update(g);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

//    @GET
//    @Path("{id}")
//    @Produces("application/json;charset=utf-8")
//    public TOGender gender(@PathParam("id") String id)throws Exception{
//        return BOGender.getGender(Integer.parseInt(id));
//    }

    @GET
    @Produces("application/json;charset=utf-8")
    public List<TOGender> genders()throws Exception{
        Object r = Cache.getCache("gender", "list");

        List<TOGender> l;

        if (r != null) {
            l = (List<TOGender>)r;
        } else{
            l = BOGender.listGender();
            Cache.setCache("gender", "list", l, 120);
        }

        return l;
    }

    @DELETE
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public void delete(@PathParam("id") String idGender) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")) {
                    BOGender.delete(Integer.parseInt(idGender));
                } else {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        }
        else{
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
