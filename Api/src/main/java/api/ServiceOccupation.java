package api;

import bo.BOOccupation;
import bo.BOUser;
import fw.Cache;
import to.TOOccupation;
import to.TOUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("occupation")
public class ServiceOccupation {

    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    @Produces("application/json;charset=utf-8")
    public void insert(TOOccupation o) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOOccupation.insert(o);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }


    @PUT
    @Consumes("application/json;charset=utf-8")
    public void update(TOOccupation o) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")){
                    BOOccupation.update(o);
                    response.sendError(HttpServletResponse.SC_ACCEPTED);
                } else{
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

//    @GET
//    @Path("{id}")
//    @Produces("application/json;charset=utf-8")
//    public TOOccupation occupation(@PathParam("id") String id)throws Exception{
//        return BOOccupation.getOccupation(Integer.parseInt(id));
//    }

    @GET
    @Produces("application/json;charset=utf-8")
    public List<TOOccupation> occupations()throws Exception{
        Object r = Cache.getCache("occupation", "list");

        List<TOOccupation> l;

        if (r != null) {
            l = (List<TOOccupation>)r;
        } else{
            l = BOOccupation.listOccupation();
            Cache.setCache("occupation", "list", l, 120);
        }

        return l;
    }

    @DELETE
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public void delete(@PathParam("id") String idOccupation) throws Exception {
        Object r = Cache.getCache("user", "token");

        if (r != null) {
            if (BOUser.isValidToken(r.toString())) {

                TOUser t = BOUser.me(r.toString());

                if (t.getTypeuserID() != null && t.getTypeuserID().equals("48gl53eio1sft1dck7ml4a")) {
                    BOOccupation.delete(Integer.parseInt(idOccupation));
                } else {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        }
        else{
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
