package dao;

import fw.Data;
import to.TOPersona;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOPersona {
    public static void insert(Connection c, TOPersona t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" insert into persona (name) values (?) ");

        Data.executeUpdate(c, sql.toString(), t.getName());

    }

    public static void update(Connection c, TOPersona t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" update persona set name = ? where id = ? ");

        Data.executeUpdate(c, sql.toString(), t.getName(), t.getId());

    }

    public static void delete(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from persona where id = ? ");

        Data.executeUpdate(c, sql.toString(), id);

    }

    public static TOPersona getPersona(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, name from persona where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                TOPersona t = new TOPersona();
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
                return t;
            } else {
                return null;
            }

        }
    }

    public static List<TOPersona> listPersona(Connection c, String search) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, name from persona ");
        sql.append(" where name like concat('%', ?, '%') ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), search)) {

            List<TOPersona> l = new ArrayList<>();

            while (rs.next()) {

                TOPersona t = new TOPersona();
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
                l.add(t);
            }

            return l;

        }
    }
}
