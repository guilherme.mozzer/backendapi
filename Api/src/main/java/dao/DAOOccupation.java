package dao;

import fw.Data;
import to.TOOccupation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOOccupation {
    public static void insert(Connection c, TOOccupation t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" insert into occupation (description) values (?) ");

        Data.executeUpdate(c, sql.toString(), t.getDescription());

    }

    public static void update(Connection c, TOOccupation t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" update occupation set description = ? where id = ? ");

        Data.executeUpdate(c, sql.toString(), t.getDescription(), t.getId());

    }

    public static void delete(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from occupation where id = ? ");

        Data.executeUpdate(c, sql.toString(), id);

    }

//    public static TOOccupation getOccupation(Connection c, int id) throws Exception {
//        StringBuilder sql = new StringBuilder();
//        sql.append(" select id, description from occupation where id = ? ");
//
//        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {
//
//            if (rs.next()) {
//                TOOccupation t = new TOOccupation();
//                t.setId(rs.getInt("id"));
//                t.setDescription(rs.getString("description"));
//                return t;
//            } else {
//                return null;
//            }
//
//        }
//    }

    public static List<TOOccupation> listOccupation(Connection c) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, description from occupation ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {

            List<TOOccupation> l = new ArrayList<>();

            while (rs.next()) {

                TOOccupation t = new TOOccupation();
                t.setId(rs.getInt("id"));
                t.setDescription(rs.getString("description"));
                l.add(t);
            }

            return l;
        }
    }

}
