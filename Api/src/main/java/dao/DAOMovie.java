package dao;

import fw.Data;
import to.TOMovie;
import to.TOMovieGender;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOMovie {

    public static void insert (Connection c, TOMovie t)throws Exception{
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into movie (title, synopsis, studioID, categoryID) ");
        sql.append(" values (?, ?, ?, ?) ");

        Data.executeUpdate(c, sql.toString(), t.getTitle(), t.getSynopsis(), t.getStudioID(), t.getCategoryID());
    }

    public static void update (Connection c, TOMovie t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" update movie ");
        sql.append(" set title = ?, synopsis = ?, studioID = ?, categoryID = ? ");
        sql.append(" where id = ? ");

        Data.executeUpdate(c, sql.toString(), t.getTitle(), t.getSynopsis(), t.getStudioID(), t.getCategoryID(), t.getId());
    }

    public static void delete(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from movie where id = ? ");

        Data.executeUpdate(c, sql.toString(), id);

    }

    public static TOMovie getMovie(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, title, synopsis, studioID, categoryID ");
        sql.append(" from movie where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                TOMovie t = new TOMovie();
                t.setId(rs.getInt("id"));
                t.setTitle(rs.getString("title"));
                t.setSynopsis(rs.getString("synopsis"));
                t.setStudioID(rs.getInt("studioID"));
                t.setCategoryID(rs.getInt("categoryID"));
                return t;
            } else {
                return null;
            }

        }
    }

    public static List<TOMovie> listMovie(Connection c) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, title, synopsis, studioID, categoryID ");
        sql.append(" from movie ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {

            List<TOMovie> l = new ArrayList<>();

            while (rs.next()) {
                TOMovie t = new TOMovie();
                t.setId(rs.getInt("id"));
                t.setTitle(rs.getString("title"));
                t.setSynopsis(rs.getString("synopsis"));
                t.setStudioID(rs.getInt("studioID"));
                t.setCategoryID(rs.getInt("categoryID"));
                l.add(t);
            }

            return l;

        }
    }

    public static List<TOMovie> listMovieByTitle(Connection c, String search) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, title, synopsis, studioID, categoryID ");
        sql.append(" from movie ");
        sql.append(" where title like concat('%', ?, '%') ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), search)) {

            List<TOMovie> l = new ArrayList<>();

            while (rs.next()) {
                TOMovie t = new TOMovie();
                t.setId(rs.getInt("id"));
                t.setTitle(rs.getString("title"));
                t.setSynopsis(rs.getString("synopsis"));
                t.setStudioID(rs.getInt("studioID"));
                t.setCategoryID(rs.getInt("categoryID"));
                l.add(t);
            }

            return l;

        }
    }

    public static List<TOMovie> listMovieByCategory(Connection c, int categoryID) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, title, synopsis, studioID, categoryID ");
        sql.append(" from movie ");
        sql.append(" where categoryID = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), categoryID)) {

            List<TOMovie> l = new ArrayList<>();

            while (rs.next()) {
                TOMovie t = new TOMovie();
                t.setId(rs.getInt("id"));
                t.setTitle(rs.getString("title"));
                t.setSynopsis(rs.getString("synopsis"));
                t.setStudioID(rs.getInt("studioID"));
                t.setCategoryID(rs.getInt("categoryID"));
                l.add(t);
            }

            return l;
        }
    }

    public static List<TOMovieGender> listMovieByGender(Connection c, int genderID) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select m.id as movieID, m.title as title, m.synopsis as synopsis, g.id as genderID, g.name as gender ");
        sql.append(" from movie m join genderlist gl ");
        sql.append(" on m.id = gl.movieID ");
        sql.append(" join gender g ");
        sql.append(" on g.id = gl.genderID ");
        sql.append(" and g.id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), genderID)) {

            List<TOMovieGender> l = new ArrayList<>();

            while (rs.next()) {
                TOMovieGender t = new TOMovieGender();
                t.setMovieID(rs.getInt("movieID"));
                t.setTitle(rs.getString("title"));
                t.setSynopsis(rs.getString("synopsis"));
                t.setGenderID(rs.getInt("genderID"));
                t.setGender(rs.getString("gender"));
                l.add(t);
            }

            return l;
        }
    }
}
