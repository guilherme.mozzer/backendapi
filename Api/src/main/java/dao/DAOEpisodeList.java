package dao;

import fw.Data;
import to.TOEpisodeList;
import to.TOEpisodeUserList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOEpisodeList {

    public static void insert(Connection c, TOEpisodeList t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into episodelist (episodeID, userID, status) ");
        sql.append(" values (?, ?, ?) ");

        Data.executeUpdate(c, sql.toString(), t.getEpisodeID(), t.getUserID(), t.getStatus());
    }

    public static void delete(Connection c, int episodeID, String userID) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from episodelist where episodeID = ? and userID = ? ");

        Data.executeUpdate(c, sql.toString(), episodeID, userID);

    }

    public static List<TOEpisodeUserList> getEpisodeListByUser(Connection c, String userID) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select e.seasonID as seasonID, e.movieID as movieID, e.id as episodeID, e.title as title, el.status as status ");
        sql.append(" from episode e join episodelist el ");
        sql.append(" on e.id = el.episodeID ");
        sql.append(" and el.userID = ? and el.status = 1; ");

        try(ResultSet rs = Data.executeQuery(c, sql.toString(), userID)){
            List<TOEpisodeUserList> l = new ArrayList<>();

            while (rs.next()){
                TOEpisodeUserList t = new TOEpisodeUserList();
                t.setSeasonID(rs.getInt("seasonID"));
                t.setMovieID(rs.getInt("movieID"));
                t.setEpisodeID(rs.getInt("episodeID"));
                t.setTitle(rs.getString("title"));
                t.setStatus(rs.getBoolean("status"));
                l.add(t);
            }

            return l;
        }
    }

    public static List<TOEpisodeUserList> getEpisodeListByUserAndMovie(Connection c, String userID, int movieID) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select e.seasonID as seasonID, e.movieID as movieID, e.id as episodeID, e.title as title, el.status as status ");
        sql.append(" from episode e join episodelist el ");
        sql.append(" on e.id = el.episodeID ");
        sql.append(" and e.movieID = ? ");
        sql.append(" and el.userID = ? and el.status = 1; ");
        try(ResultSet rs = Data.executeQuery(c, sql.toString(), movieID, userID)){
            List<TOEpisodeUserList> l = new ArrayList<>();

            while (rs.next()){
                TOEpisodeUserList t = new TOEpisodeUserList();
                t.setSeasonID(rs.getInt("seasonID"));
                t.setMovieID(rs.getInt("movieID"));
                t.setEpisodeID(rs.getInt("episodeID"));
                t.setTitle(rs.getString("title"));
                t.setStatus(rs.getBoolean("status"));
                l.add(t);
            }

            return l;
        }
    }

    public static List<TOEpisodeUserList> getEpisodeListByUserAndMovieAndSeason(Connection c, String userID, int movieID, int seasonID) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select e.seasonID as seasonID, e.movieID as movieID, e.id as episodeID, e.title as title, el.status as status ");
        sql.append(" from episode e join episodelist el ");
        sql.append(" on e.id = el.episodeID ");
        sql.append(" and e.movieID = ? and e.seasonID = ?");
        sql.append(" and el.userID = ? and el.status = 1; ");
        try(ResultSet rs = Data.executeQuery(c, sql.toString(), movieID, seasonID, userID)){
            List<TOEpisodeUserList> l = new ArrayList<>();

            while (rs.next()){
                TOEpisodeUserList t = new TOEpisodeUserList();
                t.setSeasonID(rs.getInt("seasonID"));
                t.setMovieID(rs.getInt("movieID"));
                t.setEpisodeID(rs.getInt("episodeID"));
                t.setTitle(rs.getString("title"));
                t.setStatus(rs.getBoolean("status"));
                l.add(t);
            }

            return l;
        }
    }
}
