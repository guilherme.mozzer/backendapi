package dao;

import fw.Data;
import to.TOGender;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOGender {


    public static void insert(Connection c, TOGender t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" insert into gender (name) values (?) ");

        Data.executeUpdate(c, sql.toString(), t.getName());

    }

    public static void update(Connection c, TOGender t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" update gender set name = ? where id = ? ");

        Data.executeUpdate(c, sql.toString(), t.getName(), t.getId());

    }

    public static void delete(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from gender where id = ? ");

        Data.executeUpdate(c, sql.toString(), id);

    }

//    public static TOGender getGender(Connection c, int id) throws Exception {
//        StringBuilder sql = new StringBuilder();
//        sql.append(" select id, name from gender where id = ? ");
//
//        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {
//
//            if (rs.next()) {
//                TOGender t = new TOGender();
//                t.setId(rs.getInt("id"));
//                t.setName(rs.getString("name"));
//                return t;
//            } else {
//                return null;
//            }
//
//        }
//    }

    public static List<TOGender> listGender(Connection c) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, name from gender ");
//        sql.append(" where name like concat('%', ?, '%') ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {

            List<TOGender> l = new ArrayList<>();

            while (rs.next()) {

                TOGender t = new TOGender();
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
                l.add(t);
            }

            return l;
        }
    }

}
