package dao;

import fw.Data;
import to.TOStudio;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOStudio {

    public static void insert(Connection c, TOStudio t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" insert into studio (name) values (?) ");

        Data.executeUpdate(c, sql.toString(), t.getName());

    }

    public static void update(Connection c, TOStudio t) throws Exception {

       StringBuilder sql = new StringBuilder();
       sql.append(" update studio set name = ? where id = ? ");

       Data.executeUpdate(c, sql.toString(), t.getName(), t.getId());

    }

    public static void delete(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from studio where id = ? ");

        Data.executeUpdate(c, sql.toString(), id);

    }

    public static TOStudio getStudio(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
       sql.append(" select id, name from studio where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                TOStudio t = new TOStudio();
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
                return t;
            } else {
                return null;
            }

        }
    }

    public static List<TOStudio> listStudio(Connection c) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, name from studio ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {

            List<TOStudio> l = new ArrayList<>();

            while (rs.next()) {

                TOStudio t = new TOStudio();
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
                l.add(t);
            }

            return l;
        }
    }

}
