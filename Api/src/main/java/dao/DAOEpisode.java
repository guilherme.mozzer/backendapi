package dao;

import fw.Data;
import to.TOEpisode;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOEpisode {

    public static void insert (Connection c, TOEpisode t)throws Exception{
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into episode (title, duration, cronology, seasonID, movieID) ");
        sql.append(" values (?, ?, ?, ?, ?) ");

        Data.executeUpdate(c, sql.toString(), t.getTitle(), t.getDuration(), t.getCronology(), t.getSeasonID(), t.getMovieID());
    }

    public static void update (Connection c, TOEpisode t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" update episode ");
        sql.append(" set title = ?, duration = ?, cronology = ?, seasonID = ?, movieID = ? ");
        sql.append(" where id = ? ");

        Data.executeUpdate(c, sql.toString(), t.getTitle(), t.getDuration(), t.getCronology(), t.getSeasonID(), t.getMovieID(), t.getId());
    }

    public static void delete(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from episode where id = ? ");

        Data.executeUpdate(c, sql.toString(), id);

    }

    /*public static TOEpisode getEpisode(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, title, duration, cronology, seasonID, movieID ");
        sql.append(" from episode where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                TOEpisode t = new TOEpisode();
                t.setId(rs.getInt("id"));
                t.setTitle(rs.getString("title"));
                t.setDuration(rs.getInt("duration"));
                t.setCronology(rs.getInt("cronology"));
                t.setSeasonID(rs.getInt("seasonID"));
                t.setMovieID(rs.getInt("movieID"));
                return t;
            } else {
                return null;
            }

        }
    }*/

    public static List<TOEpisode> listEpisodeByMovie(Connection c, int movieID) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, title, duration, cronology, seasonID, movieID ");
        sql.append(" from episode ");
        sql.append(" where movieID = ? ");
        sql.append(" order by cronology ");
        try (ResultSet rs = Data.executeQuery(c, sql.toString(), movieID)) {

            List<TOEpisode> l = new ArrayList<>();

            while (rs.next()) {
                TOEpisode t = new TOEpisode();
                t.setId(rs.getInt("id"));
                t.setTitle(rs.getString("title"));
                t.setDuration(rs.getInt("duration"));
                t.setCronology(rs.getInt("cronology"));
                t.setSeasonID(rs.getInt("seasonID"));
                t.setMovieID(rs.getInt("movieID"));
                l.add(t);
            }

            return l;

        }
    }

    public static List<TOEpisode> listEpisodeBySeason(Connection c, int movieID, int seasonID) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, title, duration, cronology, seasonID, movieID ");
        sql.append(" from episode ");
        sql.append(" where movieID = ? and seasonID = ? ");
        sql.append(" order by cronology ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), movieID, seasonID)) {

            List<TOEpisode> l = new ArrayList<>();

            while (rs.next()) {
                TOEpisode t = new TOEpisode();
                t.setId(rs.getInt("id"));
                t.setTitle(rs.getString("title"));
                t.setDuration(rs.getInt("duration"));
                t.setCronology(rs.getInt("cronology"));
                t.setSeasonID(rs.getInt("seasonID"));
                t.setMovieID(rs.getInt("movieID"));
                l.add(t);
            }

            return l;

        }
    }
}
