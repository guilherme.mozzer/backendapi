package dao;

import fw.Data;
import to.TOCategory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOCategory {
    public static void insert(Connection c, TOCategory t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" insert into category (description) values (?) ");

        Data.executeUpdate(c, sql.toString(), t.getDescription());

    }

    public static void update(Connection c, TOCategory t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" update category set description = ? where id = ? ");

        Data.executeUpdate(c, sql.toString(), t.getDescription(), t.getId());

    }

    public static void delete(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from category where id = ? ");

        Data.executeUpdate(c, sql.toString(), id);

    }

    /*public static TOCategory getcategory(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select categoryID, description from category where categoryID = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                TOCategory t = new TOCategory();
                t.setId(rs.getString("categoryID"));
                t.setDescription(rs.getString("description"));
                return t;
            } else {
                return null;
            }

        }
    }*/

    public static List<TOCategory> listcategory(Connection c) throws Exception {
        StringBuilder sql = new StringBuilder();
       sql.append(" select id, description from category ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {

            List<TOCategory> l = new ArrayList<>();

            while (rs.next()) {

                TOCategory t = new TOCategory();
                t.setId(rs.getInt("id"));
                t.setDescription(rs.getString("description"));
                l.add(t);
            }

            return l;

        }
    }

}
