package dao;

import fw.Data;
import to.TOUser;

import java.sql.Connection;
import java.sql.ResultSet;

public class DAOUser {

    public static TOUser getByToken(Connection c, String token) throws Exception {

        StringBuilder s = new StringBuilder();
        s.append(" select id, name, email, password, token, createdat, active, expiredat, typeuserID from User ");
        s.append(" where ");
        s.append(" token = ? ");
        s.append(" and active ");

        try (ResultSet rs = Data.executeQuery(c, s.toString(), token)) {

            if (rs.next()) {
                TOUser u = new TOUser();
                u.setId(rs.getString("id"));
                u.setName(rs.getString("name"));
                u.setEmail(rs.getString("email"));
                u.setToken(rs.getString("token"));
                u.setActive(rs.getBoolean("active"));
                u.setCreatedat(rs.getTimestamp("createdat"));
                u.setExpiredat(rs.getTimestamp("expiredat"));
                u.setTypeuserID(rs.getString("typeuserID"));
                return u;
            } else {
                return null;
            }

        }
    }

    public static boolean verifyByEmail(Connection conn, TOUser account) throws Exception {

        StringBuilder sql = new StringBuilder();

        sql.append(" select email ");
        sql.append(" from User ");
        sql.append(" where active ");
        sql.append(" and email = ? and id != ?");

        try (ResultSet rs = Data.executeQuery(conn, sql.toString(), account.getEmail(), account.getId())) {
            if (!rs.next()) {
                return false;//não existe ninguuem com esse email
            }
            return true;//existe um usuario que possui esse email
        }
    }

    public static void insert(Connection c, TOUser t) throws Exception { //usuario comum cadastrando no sistema
        StringBuilder s = new StringBuilder();
        s.append(" insert into user(id, name, email, password, createdat, active) values ");
        s.append(" (?, ?, ?, ?, now(), true) ");
        Data.executeUpdate(c, s.toString(), t.getId(), t.getName(), t.getEmail(), t.getPassword());
    }

    public static void update(Connection c, TOUser t) throws Exception {
        StringBuilder s = new StringBuilder();
        s.append(" update User set name = ?, email = ?, password = ?, active = ?");
        s.append(" where id = ? ");
        Data.executeUpdate(c, s.toString(), t.getName(), t.getEmail(), t.getPassword(), t.isActive(), t.getId());
    }

    public static void updateType(Connection c, TOUser t) throws Exception {
        StringBuilder s = new StringBuilder();
        s.append(" update User set name = ?, email = ?, password = ?, active = ?, typeuserID = ? ");
        s.append(" where id = ? ");
        Data.executeUpdate(c, s.toString(), t.getName(), t.getEmail(), t.getPassword(), t.isActive(), t.getTypeuserID(), t.getId());
    }

    public static TOUser auth(Connection c, TOUser u) throws Exception {

        StringBuilder s = new StringBuilder();
        s.append(" select id, name, email, password, token, createdat, active, expiredat, typeuserID from User ");
        s.append(" where ");
        s.append(" email = ? and password = ? ");
        s.append(" and active ");

        try (ResultSet rs = Data.executeQuery(c, s.toString(), u.getEmail(), u.getPassword())) {

            if (rs.next()) {
                u = new TOUser();
                u.setId(rs.getString("id"));
                u.setName(rs.getString("name"));
                u.setEmail(rs.getString("email"));
                u.setToken(rs.getString("token"));
                u.setActive(rs.getBoolean("active"));
                u.setCreatedat(rs.getTimestamp("createdat"));
                u.setExpiredat(rs.getTimestamp("expiredat"));
                u.setTypeuserID(rs.getString("typeuserID"));
                return u;
            } else {
                return null;
            }

        }

    }

    public static void updateToken(Connection c, TOUser u) throws Exception {

        StringBuilder s = new StringBuilder();
        s.append(" update User ");
        s.append(" set token = ?, expiredat = ? ");
        s.append(" where id = ? ");

        Data.executeUpdate(c, s.toString(), u.getToken(), u.getExpiredat(), u.getId());

    }


}
