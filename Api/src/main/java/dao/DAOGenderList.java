package dao;

import fw.Data;
import to.TOGenderList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOGenderList {
    public static void insert(Connection c, TOGenderList t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into genderlist (movieID, genderID) ");
        sql.append(" values (?, ?) ");

        Data.executeUpdate(c, sql.toString(), t.getMovieID(), t.getGenderID());
    }

    public static void delete(Connection c, int movieID, int genderID) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from genderlist where movieID = ? and genderID = ?");

        Data.executeUpdate(c, sql.toString(), movieID, genderID);

    }

    public static List<TOGenderList> getGenderListMovie(Connection c, int movieID) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select movieID, genderID ");
        sql.append(" from genderlist where movieID = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), movieID)) {

            List<TOGenderList> l = new ArrayList<>();

            while (rs.next()) {
                TOGenderList t = new TOGenderList();
                t.setMovieID(rs.getInt("movieID"));
                t.setGenderID(rs.getInt("genderID"));
                l.add(t);
            }

            return l;
        }
    }

    public static List<TOGenderList> getGenderListGender(Connection c, int genderID) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select movieID, genderID ");
        sql.append(" from genderlist where genderID = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), genderID)) {

            List<TOGenderList> l = new ArrayList<>();

            while (rs.next()) {
                TOGenderList t = new TOGenderList();
                t.setMovieID(rs.getInt("movieID"));
                t.setGenderID(rs.getInt("genderID"));
                l.add(t);
            }

            return l;
        }
    }

    public static List<TOGenderList> getGenderList(Connection c) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select movieID, genderID ");
        sql.append(" from genderlist");

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {

            List<TOGenderList> l = new ArrayList<>();

            while (rs.next()) {
                TOGenderList t = new TOGenderList();
                t.setMovieID(rs.getInt("movieID"));
                t.setGenderID(rs.getInt("genderID"));
                l.add(t);
            }

            return l;
        }
    }
}
