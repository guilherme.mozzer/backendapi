package dao;

import fw.Data;
import to.TOStaff;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOStaff {

    public static void insert (Connection c, TOStaff t)throws Exception{
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into staff (name, occupationID) ");
        sql.append(" values (?, ?) ");

        Data.executeUpdate(c, sql.toString(), t.getName(), t.getOccupationID());
    }

    public static void update (Connection c, TOStaff t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" update staff ");
        sql.append(" set name = ?, occupationID = ? ");
        sql.append(" where id = ? ");

        Data.executeUpdate(c, sql.toString(), t.getName(), t.getOccupationID(),t.getId());
    }

    public static void delete(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from staff where id = ? ");

        Data.executeUpdate(c, sql.toString(), id);

    }

    public static TOStaff getStaff(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, name, occupationID ");
        sql.append(" from staff where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                TOStaff t = new TOStaff();
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
                t.setOccupationID(rs.getInt("occupationID"));
                return t;
            } else {
                return null;
            }

        }
    }

    public static List<TOStaff> listStaff(Connection c, String search) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, name, occupationID ");
        sql.append(" from staff ");
        sql.append(" where name like concat('%', ?, '%') ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), search)) {

            List<TOStaff> l = new ArrayList<>();

            while (rs.next()) {
                TOStaff t = new TOStaff();
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
                t.setOccupationID(rs.getInt("occupationID"));
                l.add(t);
            }

            return l;
        }
    }
    
}
