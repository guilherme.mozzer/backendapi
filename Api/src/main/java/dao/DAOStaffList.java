package dao;

import fw.Data;
import to.TOStaffList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOStaffList {

    public static void insert(Connection c, TOStaffList t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into stafflist (movieID, staffID, personaID) ");
        sql.append(" values (?, ?, ?) ");

        Data.executeUpdate(c, sql.toString(), t.getMovieID(), t.getStaffID(), t.getPersonaID());
    }

    public static void delete(Connection c, int movieID, int staffID, int personaID) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from stafflist where movieID = ? and staffID = ? and personaID = ?");

        Data.executeUpdate(c, sql.toString(), movieID, staffID, personaID);

    }

    public static List<TOStaffList> getStaffListMovie(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select movieID, staffID, personaID ");
        sql.append(" from stafflist where movieID = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {

            List<TOStaffList> l = new ArrayList<>();

            while (rs.next()) {
                TOStaffList t = new TOStaffList();
                t.setMovieID(rs.getInt("movieID"));
                t.setStaffID(rs.getInt("staffID"));
                t.setPersonaID(rs.getInt("personaID"));
                l.add(t);
            }

            return l;
        }
    }

    public static List<TOStaffList> getStaffListStaff(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select movieID, staffID, personaID ");
        sql.append(" from stafflist where staffID = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {

            List<TOStaffList> l = new ArrayList<>();

            while (rs.next()) {
                TOStaffList t = new TOStaffList();
                t.setMovieID(rs.getInt("movieID"));
                t.setStaffID(rs.getInt("staffID"));
                t.setPersonaID(rs.getInt("personaID"));
                l.add(t);
            }

            return l;
        }

    }

    public static List<TOStaffList> getStaffListPersona(Connection c, int personaID) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select movieID, staffID, personaID ");
        sql.append(" from stafflist where personaID = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), personaID)) {

            List<TOStaffList> l = new ArrayList<>();

            while (rs.next()) {
                TOStaffList t = new TOStaffList();
                t.setMovieID(rs.getInt("movieID"));
                t.setStaffID(rs.getInt("staffID"));
                t.setPersonaID(rs.getInt("personaID"));
                l.add(t);
            }

            return l;

        }

    }

    public static List<TOStaffList> getStaffListMovieStaff(Connection c, int movie, int staff) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select movieID, staffID, personaID ");
        sql.append(" from stafflist where movieID = ? and staffID = ?");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), movie, staff)) {

            List<TOStaffList> l = new ArrayList<>();

            while (rs.next()) {
                TOStaffList t = new TOStaffList();
                t.setMovieID(rs.getInt("movieID"));
                t.setStaffID(rs.getInt("staffID"));
                t.setPersonaID(rs.getInt("personaID"));
                l.add(t);
            }

            return l;

        }
    }


    public static List<TOStaffList> getStaffList(Connection c) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select movieID, staffID, personaID ");
        sql.append(" from stafflist");

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {

            List<TOStaffList> l = new ArrayList<>();

            while (rs.next()) {
                TOStaffList t = new TOStaffList();
                t.setMovieID(rs.getInt("movieID"));
                t.setStaffID(rs.getInt("staffID"));
                t.setPersonaID(rs.getInt("personaID"));
                l.add(t);
            }

            return l;

        }

    }
}
