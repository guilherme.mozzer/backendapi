package dao;

import fw.Data;
import to.TOSeason;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOSeason {

    public static void insert (Connection c, TOSeason t)throws Exception{
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into season (name) ");
        sql.append(" values (?) ");

        Data.executeUpdate(c, sql.toString(), t.getName());
    }

    public static void update (Connection c, TOSeason t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" update season ");
        sql.append(" set name = ? ");
        sql.append(" where id = ? ");

        Data.executeUpdate(c, sql.toString(), t.getName(), t.getId());
    }

    public static void delete(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from season where id = ? ");

        Data.executeUpdate(c, sql.toString(), id);

    }

    /*public static TOSeason getSeason(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, name ");
        sql.append(" from season where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                TOSeason t = new TOSeason();
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
                return t;
            } else {
                return null;
            }

        }
    }*/

    public static List<TOSeason> listSeason(Connection c) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, name from season ");
/*        sql.append(" where name like concat('%', ?, '%') ");*/

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {

            List<TOSeason> l = new ArrayList<>();

            while (rs.next()) {
                TOSeason t = new TOSeason();
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
                l.add(t);
            }

            return l;

        }
    }
}
