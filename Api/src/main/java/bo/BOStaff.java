package bo;

import dao.DAOStaff;
import fw.Data;
import to.TOStaff;

import java.sql.Connection;
import java.util.List;

public class BOStaff {

    public static void insert(TOStaff t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOStaff.insert(c, t);
        }
    }

    public static void update(TOStaff t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOStaff.update(c, t);
        }
    }

    public static void delete(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOStaff.delete(c, id);
        }
    }

    public static TOStaff getStaff(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOStaff.getStaff(c, id);
        }
    }

    public static List<TOStaff> listStaff(String search) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOStaff.listStaff(c, search);
        }
    }
}
