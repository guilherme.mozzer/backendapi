package bo;

import dao.DAOSeason;
import fw.Data;
import to.TOSeason;

import java.sql.Connection;
import java.util.List;

public class BOSeason {

    public static void insert(TOSeason t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOSeason.insert(c, t);
        }
    }

    public static void update(TOSeason t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOSeason.update(c, t);
        }
    }

    public static void delete(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
        DAOSeason.delete(c, id);
    }
}

/*    public static TOSeason getSeason(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOSeason.getSeason(c, id);
        }
    }*/

    public static List<TOSeason> listSeason() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOSeason.listSeason(c);
        }
    }
}
