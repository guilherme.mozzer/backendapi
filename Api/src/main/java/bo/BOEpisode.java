package bo;

import dao.DAOEpisode;
import fw.Data;
import to.TOEpisode;

import java.sql.Connection;
import java.util.List;

public class BOEpisode {

    public static void insert(TOEpisode t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOEpisode.insert(c, t);
        }
    }

    public static void update(TOEpisode t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOEpisode.update(c, t);
        }
    }

    public static void delete(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOEpisode.delete(c, id);
        }
    }

/*    public static TOEpisode getEpisode(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOEpisode.getEpisode(c, id);
        }
    }*/

    public static List<TOEpisode> listEpisodeByMovie(int movieID) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOEpisode.listEpisodeByMovie(c, movieID);
        }
    }

    public static List<TOEpisode> listEpisodeBySeason(int movieID, int seasonID) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOEpisode.listEpisodeBySeason(c, movieID, seasonID);
        }
    }
}
