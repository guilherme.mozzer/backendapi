package bo;

import dao.DAOCategory;
import fw.Data;
import to.TOCategory;

import java.sql.Connection;
import java.util.List;

public class BOCategory {
    public static void insert(TOCategory t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOCategory.insert(c, t);
        }
    }

    public static void update(TOCategory t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOCategory.update(c, t);
        }
    }

    public static void delete(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOCategory.delete(c, id);
        }
    }

/*
    public static TOCategory getCategory(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOCategory.getcategory(c, id);
        }
    }
*/

    public static List<TOCategory> listCategory() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOCategory.listcategory(c);
        }
    }
}
