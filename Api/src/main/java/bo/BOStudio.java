package bo;

import dao.DAOStudio;
import fw.Data;
import to.TOStudio;

import java.sql.Connection;
import java.util.List;

public class BOStudio {

    public static void insert(TOStudio t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOStudio.insert(c, t);
        }
    }

    public static void update(TOStudio t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOStudio.update(c, t);
        }
    }

    public static void delete(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOStudio.delete(c, id);
        }
    }

    public static TOStudio getStudio(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOStudio.getStudio(c, id);
        }
    }

    public static List<TOStudio> listStudio() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOStudio.listStudio(c);
        }
    }


}
