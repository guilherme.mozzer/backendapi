package bo;

import dao.DAOPersona;
import fw.Data;
import to.TOPersona;

import java.sql.Connection;
import java.util.List;

public class BOPersona {
    public static void insert(TOPersona t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOPersona.insert(c, t);
        }
    }

    public static void update(TOPersona t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOPersona.update(c, t);
        }
    }

    public static void delete(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOPersona.delete(c, id);
        }
    }


    public static TOPersona getPersona(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOPersona.getPersona(c, id);
        }
    }


    public static List<TOPersona> listPersona(String search) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOPersona.listPersona(c, search);
        }
    }
}
