package bo;


import dao.DAOUser;
import fw.*;
import to.TOUser;

import java.sql.Connection;

public class BOUser {

    public static TOUser me (String token) throws Exception{
        try (Connection c = Data.openConnection()) {
            return DAOUser.getByToken(c, token);
        }
    }


    public static boolean isValidToken(String token) throws Exception {
        try (Connection c = Data.openConnection()) {
            TOUser a = DAOUser.getByToken(c, token);
            if (a != null) {

                DateTime now = DateTime.now();
                if (a.getExpiredat().getTime() > now.getMillis()) {
                    return true;
                } else {
                    return false;
                }

            } else {
                return false;
            }
        }
    }

    public static boolean verifyByEmail(Connection c, TOUser a) throws Exception {
        return DAOUser.verifyByEmail(c, a);
    }

    public static TOUser insert(TOUser u) throws Exception {
        try(Connection c = Data.openConnection()){

            if(!verifyByEmail(c,u)) {
                u.setId(Guid.getString());
                u.setPassword(Encrypt.sha1(u.getPassword()));
                DAOUser.insert(c, u);

                StringBuilder message = new StringBuilder();
                message.append("Olá ").append(u.getName()).append(",<br/><br/>");
                message.append("Seja bem vindo ao MoviesApp!<br/><br/>");
                message.append("Equipe MoviesApp");

                Email email = new Email("Seja bem vindo ao MoviesApp", message.toString(), u.getEmail());
                email.start();
                return u;

            }else{
                return null;
            }
        }
    }

    public static boolean update(TOUser u) throws Exception {
        try (Connection c = Data.openConnection()) {

            if(u.getPassword() == null) {
                return false;
            }
            if(verifyByEmail(c,u)){
                return false;
            }
            if(u.getName() == null){
                return false;
            }
            u.setPassword(Encrypt.sha1(u.getPassword()));

            if(u.getTypeuserID() == null) {
                DAOUser.update(c, u);
            } else{
                DAOUser.updateType(c, u);
            }

            return true;
        }
    }

    public static TOUser auth(TOUser u) throws Exception {

        try (Connection c = Data.openConnection()) {

            u.setPassword(Encrypt.sha1(u.getPassword()));

            TOUser t = DAOUser.auth(c, u);
            if (t != null) {

                DateTime expiredAt = DateTime.now();
                expiredAt.addMinute(10);

                t.setExpiredat(expiredAt.getTimestamp());

                t.setToken(Guid.getString());
                DAOUser.updateToken(c, t);
            }

            return t;
        }

    }

}
