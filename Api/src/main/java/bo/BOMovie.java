package bo;

import dao.DAOMovie;
import fw.Data;
import to.TOMovie;
import to.TOMovieGender;

import java.sql.Connection;
import java.util.List;

public class BOMovie {

    public static void insert(TOMovie t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOMovie.insert(c, t);
        }
    }

    public static void update(TOMovie t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOMovie.update(c, t);
        }
    }

    public static void delete(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOMovie.delete(c, id);
        }
    }

    public static TOMovie getMovie(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOMovie.getMovie(c, id);
        }
    }

    public static List<TOMovie> listMovie() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOMovie.listMovie(c);
        }
    }

    public static List<TOMovie> listMovieByTitle(String search) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOMovie.listMovieByTitle(c, search);
        }
    }

    public static List<TOMovie> listMovieByCategory(int categoryID) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOMovie.listMovieByCategory(c,categoryID);
        }
    }

    public static List<TOMovieGender> listMovieByGender(int genderID) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOMovie.listMovieByGender(c, genderID);
        }
    }
}
