package bo;

import dao.DAOOccupation;
import fw.Data;
import to.TOOccupation;

import java.sql.Connection;
import java.util.List;

public class BOOccupation {
    public static void insert(TOOccupation t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOOccupation.insert(c, t);
        }
    }

    public static void update(TOOccupation t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOOccupation.update(c, t);
        }
    }

    public static void delete(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOOccupation.delete(c, id);
        }
    }

//    public static TOOccupation getOccupation(int id) throws Exception {
//        try (Connection c = Data.openConnection()) {
//            return DAOOccupation.getOccupation(c, id);
//        }
//    }

    public static List<TOOccupation> listOccupation() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOOccupation.listOccupation(c);
        }
    }
}
