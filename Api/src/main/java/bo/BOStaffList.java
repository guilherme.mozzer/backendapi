package bo;

import dao.DAOStaffList;
import fw.Data;
import to.TOStaffList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class BOStaffList {

    public static void insert(TOStaffList t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOStaffList.insert(c, t);
        }
    }

    public static void delete(int movieID, int staffID, int personaID) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOStaffList.delete(c, movieID, staffID, personaID);
        }
    }

    public static List<TOStaffList> getStaffList() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOStaffList.getStaffList(c);
        }
    }

    public static List<TOStaffList> getStaffListByStaff(int staff) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOStaffList.getStaffListStaff(c, staff);
        }
    }

    public static List<TOStaffList> getStaffListByMovie(int movie) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOStaffList.getStaffListMovie(c, movie);
        }
    }

    public static List<TOStaffList> getStaffListByMovieAndStaff(int movie, int staff) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOStaffList.getStaffListMovieStaff(c, movie, staff);
        }
    }

    public static List<TOStaffList> getStaffListByPersona(int personaID) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOStaffList.getStaffListPersona(c, personaID);
        }
    }

}