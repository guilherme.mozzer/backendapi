package bo;

import dao.DAOEpisodeList;
import fw.Data;
import to.TOEpisodeList;
import to.TOEpisodeUserList;

import java.sql.Connection;
import java.util.List;

public class BOEpisodeList {

    public static void insert(TOEpisodeList t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOEpisodeList.insert(c, t);
        }
    }

    public static void delete(int episodeID, String userID) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOEpisodeList.delete(c, episodeID, userID);
        }
    }

    public static List<TOEpisodeUserList> getEpisodeListByUser(String userID) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOEpisodeList.getEpisodeListByUser(c, userID);
        }
    }

    public static List<TOEpisodeUserList> getEpisodeListByUserAndMovie(String userID, int movieID) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOEpisodeList.getEpisodeListByUserAndMovie(c, userID, movieID);
        }
    }

    public static List<TOEpisodeUserList> getEpisodeListByUserAndMovieAndSeason(String userID, int movieID, int seasonID) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOEpisodeList.getEpisodeListByUserAndMovieAndSeason(c, userID, movieID, seasonID);
        }
    }
}
