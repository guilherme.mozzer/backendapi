package bo;

import dao.DAOGender;
import fw.Data;
import to.TOGender;

import java.sql.Connection;
import java.util.List;

public class BOGender {
    public static void insert(TOGender t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOGender.insert(c, t);
        }
    }

    public static void update(TOGender t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOGender.update(c, t);
        }
    }

    public static void delete(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOGender.delete(c, id);
        }
    }

//    public static TOGender getGender(int id) throws Exception {
//        try (Connection c = Data.openConnection()) {
//            return DAOGender.getGender(c, id);
//        }
//    }

    public static List<TOGender> listGender() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOGender.listGender(c);
        }
    }
}
