package bo;

import dao.DAOGenderList;
import fw.Data;
import to.TOGenderList;

import java.sql.Connection;
import java.util.List;

public class BOGenderList {

    public static void insert(TOGenderList t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOGenderList.insert(c, t);
        }
    }

    public static void delete(int movieID, int genderID) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOGenderList.delete(c, movieID, genderID);
        }
    }

    public static List<TOGenderList> getGenderList() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOGenderList.getGenderList(c);
        }
    }

    public static List<TOGenderList> getGenderListMovie(int movieID) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOGenderList.getGenderListMovie(c, movieID);
        }
    }

    public static List<TOGenderList> getGenderListGender(int genderID) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOGenderList.getGenderListGender(c, genderID);
        }
    }
}
