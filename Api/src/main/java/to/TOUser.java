package to;

import java.sql.Timestamp;

public class TOUser {

    private String id;
    private String name;
    private String email;
    private String password;
    private String token;
    private boolean active;
    private Timestamp createdat;
    private Timestamp expiredat;
    private String typeuserID;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Timestamp getCreatedat() {
        return createdat;
    }

    public void setCreatedat(Timestamp createdat) {
        this.createdat = createdat;
    }

    public Timestamp getExpiredat() {
        return expiredat;
    }

    public void setExpiredat(Timestamp expiredat) {
        this.expiredat = expiredat;
    }

    public String getTypeuserID() {
        return typeuserID;
    }

    public void setTypeuserID(String typeuserID) {
        this.typeuserID = typeuserID;
    }

}
